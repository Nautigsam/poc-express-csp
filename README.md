# poc-express-csp

This example presents a limitation of Content Security Policy `connect-src` directive.

The client-side script tries to access 3 different APIs:
- local authorized API: `/api/authorized/hello`
- local forbidden API: `/api/forbidden`
- remote API: `https://api.jokes.one/jod`

We can see that we can access some APIs depending on the value of the `Content-Security-Policy` header. These are the different alternatives that can be tested in this example:

### No CSP header

If there is no CSP header set, all APIs can be accessed.

### Local-only CSP header

If the CSP header is set as `connect-src http://localhost:3000`, only the local APIs can be accessed, but this does not prevent the script from accessing local forbidden API.

### Local-only CSP header filtered on path

If the CSP header is set as `connect-src http://localhost:3000/api/authorized/`, only the local authorized API can be accessed.

This supports the idea that CSP can be used to restrict client-side requests to a part of a local API.