"use strict";

const path = require("path");
const express = require("express");

const app = express();

// This one works for external API but not for internal forbidden API
function getHostSource() {
  return "http://localhost:3000";
}

// This one filters on API route
function getAPISource() {
  return "http://localhost:3000/api/authorized/";
}

app.get("/", (req, res) => {
  let cspSource;

  //cspSource = getHostSource();
  cspSource = getAPISource();

  res.setHeader("Content-Security-Policy", `connect-src ${cspSource}`);
  res.sendFile(path.join(__dirname, "index.html"));
});

app.get("/api/authorized/hello", (req, res) => {
  res.json({ message: "Hello!" });
});
app.get("/api/forbidden", (req, res) => {
  res.json({ message: "You should never see me!" });
});

app.listen(3000, () => {
  console.log("listening on port 3000...");
});
